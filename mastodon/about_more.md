Herzlich willkommen auf freiburg.social, einer Mastodon-Instanz für Menschen aus Freiburg und Umland und alle, die sich mit der Region verbunden fühlen. Sie wird betrieben durch den Verein freiburg.social und ist durch freiwillige Spenden finanziert. Die Nutzing ist und wird für immer kostenlos bleiben.

## Regeln (Code of Conduct)

freiburg.social soll ein Ort sein auf dem Menschen offen und tolerant miteinander umgehen. Um dies zu gewährleisten moderieren wir diese Instanz aktiv und behalten uns vor Inhalte, Benutzerkonten oder sogar ganze Instanzen zu blockieren. Als Grundlage solcher Entscheidungen dienen folgende Regeln:

### Verhalten

    * Keine rassistische, sexistische, homophobe, transphobe, beeinträchtigtenfeindliche und anderweitig diskriminierende oder hasserfüllte Sprache.
    * Kein Belästigung einzelner Personen durch ungewünschte Kontaktaufnahme, Stalking oder [Doxing](https://de.wikipedia.org/wiki/Doxing).
    * Keine Verbreitung von Inhalten die in der Bundesrepublik Deutschland gegen geltendes Recht verstoßen.
    * Kein Verbreiten von Verschwörungserzählungen.

### Inhalte

    * Nicht jugendfreie Beiträge müssen als solche gekennzeichnet werden.
    * Profilbilder und Banner, die nicht jugendfrei sind, werden nicht toleriert.
    * Werbung muss als solche gekennzeichnet sein. Übermäßige oder automatisierte Werbung wird nicht toleriert.
    * Die lokale Timeline soll den manuellen Posts der Mitglieder:innen von freiburg.social vorbehalten bleiben. Automatisierte Posts, wie zum Beispiel Re-posts von Twitter, müssen auf "ungelistet" gesetzt werden. 
    * Bot-Accounts, also Nutzerkonten, die größtenteils automatisierte Inhalte erstellen, müssen als solche gekennzeichnet sein, dürfen nicht auf der lokalen Timeline posten und dürfen nur mit Nutzer:innen interagieren, wenn sie dazu eingeladen worden sind.

## Moderationsteam

Diese Instanz wird derzeit moderiert durch:

    * [@bastian](https://freiburg.social/@bastian)
    * [@NiklasMM](https://freiburg.social/@NiklasMM)

Alle aktiven Moderationsmaßnahmen, sowie Löschungen der Bans werden durch den Account [@admin](https://freiburg.social/@admin) ausgelöst.

Ihr könnt Inhalte über die in Mastodon eingebauten Melde-Werkzeuge melden oder euch direkt an einen der oben aufgelisteten Accounts wenden.

## Aktuelle Instanzsperrungen

### Stummgeschaltete Server

Beiträge von diesen Servern werden niemals auf der föderierten Timeline angezeigt, wenn du Accounts von diesen Servern folgst, kannst du sie aber uneingeschränkt sehen.

### Blockierte Server

Beiträge von diesen Servern werden durch diese Instanz nicht angenommen. Du kannst Accounts auf diesen Servern nicht folgen und auch nicht mit ihnen interagieren.
