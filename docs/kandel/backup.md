# Backup

Für das Backup des Mastodon-Systems legen wir jede Nacht einen LXC snapshot an und erstellen ein Borg-Archiv auf platform.gxis.de im Repository `/home/borg/repos/mastodon`

Dazu wird das script `/root/backup.sh` per cron job ausgeführt. Dabei wird aller output nach `/root/backup.log` geschrieben:

`0 4 * * * sh /root/backup.sh >> /root/backup.log 2>&1`

## Verschlüsselung

Wir verwenden den Verschlüsselungsmodus `keyfile` mit leerer Passphrase. Dass ist laut [1] möglich und empfohlen für automatisierte Backups.

Der Verschlüsselungs-Key liegt unter `/root/.config/borg/keys/borg_temp__home_borg_repos_mastodon`. Admins wird nahegelegt, ein backup zu ziehen.

## Vorhaltung

Die backups werden täglich erstellt und dann durch `borg prune` nachträglich so ausgedünnt, dass zunächst für 7 Tage je eins vorgehalten wird, dann für 4 Wochen jeweils eins und dann für 6 Monate jeweils eins.

## Weiterer Plan / TODOs

- Logrotate für das backup log einrichten
- Wenn das backup script irgendwie vor dem Löschen des snapshots abbricht, kann es nicht wieder ausgeführt werden, da es einen Fehler beim erstellen des Snapshots der schon existiert gibt. Das muss robuster gestaltet werden.
- Weniger daten sichern. Das sichern des ganzen Snapshots ist nicht notwenig: [2] beschreibt, was man für ein gutes Mastodon backup sichern muss.

***

[1]: https://borgbackup.readthedocs.io/en/stable/faq.html#how-can-i-specify-the-encryption-passphrase-programmatically
[2]: https://docs.joinmastodon.org/admin/backups/

